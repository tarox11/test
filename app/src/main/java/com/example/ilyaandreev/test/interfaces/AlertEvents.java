package com.example.ilyaandreev.test.interfaces;

public interface AlertEvents {

    void onPositiveClick(int which);

    void onNegative();
}
