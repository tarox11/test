package com.example.ilyaandreev.test.tools;

import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;

public class SimpleTimer {

    public interface SimpleTimerEvents {

        void timerStarted();

        void timerTicked(int timeLeft, int tickLeft);

        void timerDone();

    }

    private SimpleTimerEvents listener;

    public void setListener(SimpleTimerEvents listener) {
        this.listener = listener;
    }

    int duration = 0;
    int tickInterval = 0;
    int timeLeft = 0;

    /**
     * @param duration     millis
     * @param tickInterval millis
     */
    public SimpleTimer(int duration, int tickInterval) {
        this.duration = duration;
        this.tickInterval = tickInterval;
        this.timeLeft = duration;
    }

    boolean cancelled = false;
    boolean paused = false;

    public void start() {
        cancelled = false;
        paused = false;
        timerFinished = false;
        try {
            if (!timerThread.isAlive()) {
                timerThread.start();
            }
            if (listener != null)
                listener.timerStarted();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean timerIsStarted() {
        return timerThread.isAlive();
    }

    private boolean timerFinished = false;

    public boolean timerIsFinished() {
        return timerFinished;
    }

    public boolean timerIsPaused() {
        return paused;
    }

    public void pause() {
        paused = true;
    }

    public void cancel() {
        cancelled = true;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    if (listener != null)
                        listener.timerStarted();
                    break;
                case 1:
                    if (listener != null)
                        listener.timerTicked(timeLeft, timeLeft / tickInterval);
                    break;
                case 2:
                    timerFinished = true;
                    if (listener != null)
                        listener.timerDone();
                    break;
            }
        }
    };

    private Thread timerThread = new Thread(new Runnable() {
        @Override
        public void run() {
            int i = 0;
            while (!cancelled && timeLeft > 0) {
                if (!paused) {
                    if (i == 0) {
                        handler.sendEmptyMessage(0);
                    } else {
                        handler.sendEmptyMessage(1);
                    }
                    SystemClock.sleep(tickInterval);
                    i++;
                    timeLeft -= tickInterval;
                }
            }
            if (timeLeft == 0 || timeLeft < 0) {
                handler.sendEmptyMessage(2);
            }

        }
    });

}
