package com.example.ilyaandreev.test.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ilyaandreev.test.R;
import com.example.ilyaandreev.test.ui.base.BaseActivityWithPresenter;
import com.yandex.metrica.profile.UserProfile;

import java.util.Calendar;

import static android.provider.Settings.System.getString;

public class SplashActivity extends BaseActivityWithPresenter<SplashActivityPresenter> implements SplashActivityEvents.View  {

    private TextView txtGreeting;
    private ImageView imgBackground;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        txtGreeting = findViewById(R.id.txtGreeting);
        imgBackground = findViewById(R.id.imgBackground);
        setGreeting();


    }

    private void setGreeting() {
        int hourOfDay = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        String greeting = getString(R.string.good_afternoon);
        if (hourOfDay >= 0 && hourOfDay < 5) {
            greeting = getString(R.string.good_night);
        }
        if (hourOfDay >= 5 && hourOfDay < 11) {
            greeting = getString(R.string.good_morning);
        }
        if (hourOfDay >= 11 && hourOfDay < 18) {
            greeting = getString(R.string.good_afternoon);
        }
        if (hourOfDay >= 18) {
            greeting = getString(R.string.good_evening);
        }

        if (hourOfDay >= 5 && hourOfDay < 18) {
            imgBackground.setImageResource(R.drawable.ic_cat);
        } else {
            imgBackground.setImageResource(R.drawable.ic_cat_appealing);
        }

        txtGreeting.setText(greeting);
    }

    @Override
    public void showMainScreen() {
        startActivity(new Intent(this, MainActivity.class));
    }
}
