package com.example.ilyaandreev.test.ui.users;

import com.example.ilyaandreev.test.models.Repository;
import com.example.ilyaandreev.test.models.User;
import com.example.ilyaandreev.test.models.responses.ResponseHandler;
import com.example.ilyaandreev.test.models.responses.ServerResponse;
import com.example.ilyaandreev.test.mvp.implementation.BasePresenterImpl;
import com.example.ilyaandreev.test.tools.Tools;

import java.util.List;

import javax.inject.Inject;

public class UserItemPresenter extends BasePresenterImpl<UserItemEvents.View> implements UserItemEvents.Presenter {

    @Inject
    public UserItemPresenter() {
    }


    User user;

    List<Repository> repositories;

    @Override
    public void loadRepository(String userName) {
        addLoadOperation(new ResponseHandler<List<Repository>>() {
            @Override
            public void success(List<Repository> response) {
                if (response != null) {
                    repositories = response;
                    getView().showRepositories(response);
                }
            }

            @Override
            public void businessError(List<Repository> response) {

            }

            @Override
            public void connectionError(String message) {


            }
        }, apiDataClient.getRepositories(userName), true, true);
    }

    @Override
    public void loadUser(String userName) {

        User user = databaseHelper.restoreUser(userName);
        if (user != null) {
            getView().showUser(user);
        }

        List<Repository> repositories = databaseHelper.restoreRepositories(userName);

        if (repositories != null) {
            getView().showRepositories(repositories);
        }
        addLoadOperation(new ResponseHandler<User>() {
            @Override
            public void success(User user) {
                if (user != null) {
                    getView().showUser(user);
                    UserItemPresenter.this.user = user;
                }
            }

            @Override
            public void businessError(User user) {

            }

            @Override
            public void connectionError(String message) {


            }
        }, apiDataClient.getUser(userName), true, true);
    }

    @Override
    public void saveUser() {
        if (user != null) {
            databaseHelper.storeUser(user);
        }
        if (repositories != null) {
            databaseHelper.storeRepositories(repositories);
        }


    }


}
