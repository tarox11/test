package com.example.ilyaandreev.test.interfaces;


import com.example.ilyaandreev.test.models.Repository;
import com.example.ilyaandreev.test.models.User;
import com.example.ilyaandreev.test.models.responses.ServerResponse;

import java.util.List;

import javax.inject.Singleton;

import rx.Observable;



@Singleton
public interface ApiDataInterface {

    Observable<ServerResponse> searchUsers(String search);

    Observable<List<Repository>> getRepositories(String userName);

    Observable<User> getUser(String userName);
}
