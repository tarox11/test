package com.example.ilyaandreev.test.ui.users;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ilyaandreev.test.R;
import com.example.ilyaandreev.test.adapters.UsersRecyclerAdapter;
import com.example.ilyaandreev.test.interfaces.AlertEvents;
import com.example.ilyaandreev.test.models.User;
import com.example.ilyaandreev.test.tools.Tools;
import com.example.ilyaandreev.test.ui.base.BaseFragmentWithPresenter;

import java.util.List;

public class UserListFragment extends BaseFragmentWithPresenter<UserListEvents.Presenter> implements UserListEvents.View, UsersRecyclerAdapter.OnNewsItemClickListener {


    UsersRecyclerAdapter adapter;
    RecyclerView recyclerView;


    View view;

    AppCompatEditText edtSearch;


    public static UserListFragment newInstance() {

        Bundle args = new Bundle();
        UserListFragment fragment = new UserListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.user_list_fragment, null);
            recyclerView = view.findViewById(R.id.rv);
            edtSearch = view.findViewById(R.id.edtSearch);
            init();

        }


        return view;
    }


    private void init() {


        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        adapter = new UsersRecyclerAdapter();
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        adapter.setOnNewClickListener(this);
        presenter.attachView(this);

        adapter.setItems(presenter.getUsers());
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() != 0) {
                    presenter.searchUser(s.toString());
                } else {
                    adapter.setItems(presenter.getUsers());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }


    @Override
    public void showConnectionError() {
        Tools.showAlert(getContext(), getString(R.string.error), getString(R.string.connection_error), getString(R.string.yes), getString(R.string.no), new AlertEvents() {
            @Override
            public void onPositiveClick(int which) {

            }

            @Override
            public void onNegative() {

            }
        });
    }

    @Override
    public void showUsers(List<User> users) {

        if (adapter == null) {
            adapter = new UsersRecyclerAdapter();
        }
        adapter.setItems(users);

    }

    @Override
    public void onItemClick(User user) {

        Bundle bundle = new Bundle();
        bundle.putString("userName", user.getLogin());
        activity.replaceFragment(UserItemFragment.newInstance(bundle));

    }


    @Override
    public void onResume() {
        super.onResume();
        edtSearch.setText(null);

    }
}
