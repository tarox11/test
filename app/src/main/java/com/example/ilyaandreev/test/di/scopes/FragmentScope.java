package com.example.ilyaandreev.test.di.scopes;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@FragmentScope
@Retention(RetentionPolicy.RUNTIME)
public @interface FragmentScope {

}