package com.example.ilyaandreev.test.di.modules;

import com.example.ilyaandreev.test.di.scopes.FragmentScope;
import com.example.ilyaandreev.test.ui.users.UserItemFragment;
import com.example.ilyaandreev.test.ui.users.UserListFragment;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public interface MainActivityModule {


    @FragmentScope
    @ContributesAndroidInjector(modules = {UserListModule.class})
    UserListFragment userListFragment();


    @FragmentScope
    @ContributesAndroidInjector(modules = {UserItemModule.class})
    UserItemFragment userItemFragment();


}
