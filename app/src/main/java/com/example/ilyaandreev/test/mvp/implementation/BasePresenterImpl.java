package com.example.ilyaandreev.test.mvp.implementation;

import com.example.ilyaandreev.test.interfaces.ApiDataInterface;
import com.example.ilyaandreev.test.interfaces.DatabaseHelperInterface;
import com.example.ilyaandreev.test.models.responses.ResponseHandler;
import com.example.ilyaandreev.test.mvp.interfaces.BasePresenter;
import com.example.ilyaandreev.test.mvp.interfaces.BaseView;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class BasePresenterImpl<V extends BaseView> implements BasePresenter<V> {

    private Subscription subscription;

    private CompositeSubscription compositeSubscription;


    @Inject
    protected DatabaseHelperInterface databaseHelper;

    @Inject
    protected ApiDataInterface apiDataClient;

    private V view;

    @Override
    public void attachView(V view) {
        this.view = view;
    }

    @Override
    public void detachView(V view) {
        this.view = null;
    }

    @Override
    public void resume(V view) {
        this.view = view;
    }

    @Override
    public boolean viewIsReady() {
        return view != null;
    }

    @Override
    public V getView() {
        return view;
    }

    @Override
    public void destroy() {
        if (compositeSubscription != null && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
            compositeSubscription = null;
        }
    }

    protected <T> void replaceLoadOperation(final ResponseHandler<T> handler, Observable observable, final boolean showProgress, final boolean showError) {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
        subscription = createLoadOperation(handler, observable, showProgress, showError);
    }

    protected <T> void addLoadOperation(final ResponseHandler<T> handler, Observable observable, final boolean showProgress, final boolean showError) {
        if (compositeSubscription == null) {
            compositeSubscription = new CompositeSubscription();
        }
        compositeSubscription.add(createLoadOperation(handler, observable, showProgress, showError));
    }


    protected <T> Subscription createLoadOperation(final ResponseHandler<T> handler, Observable<T> observable, final boolean showProgress, final boolean showError) {
        if (viewIsReady()) {
            if (showProgress) {
                getView().showProgress();
            }
        }
        return observable.subscribe(new Subscriber<T>() {

            @Override
            public void onCompleted() {
                if (viewIsReady()) {
                    getView().hideProgress();
                }
            }

            @Override
            public void onError(Throwable e) {
                if (viewIsReady()) {
                    if (showError)
                        //    getView().showError(e.getMessage());

                        handler.connectionError(e.getMessage());
                    getView().hideProgress();
                    //     Logger.d(e.getMessage());
                }
            }

            @Override
            public void onNext(T response) {
                if (viewIsReady()) {
                    handler.success(response);
                    getView().hideProgress();
                }
            }
        });
    }

}
