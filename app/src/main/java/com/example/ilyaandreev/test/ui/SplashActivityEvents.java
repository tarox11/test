package com.example.ilyaandreev.test.ui;

import com.example.ilyaandreev.test.mvp.interfaces.BasePresenter;
import com.example.ilyaandreev.test.mvp.interfaces.BaseView;

public interface SplashActivityEvents {

    interface View extends BaseView {
        void showMainScreen();
    }

    interface Presenter extends BasePresenter<View> {

    }
}
