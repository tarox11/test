package com.example.ilyaandreev.test.network;


import com.example.ilyaandreev.test.BuildConfig;
import com.example.ilyaandreev.test.interfaces.ApiDataInterface;
import com.example.ilyaandreev.test.models.Repository;
import com.example.ilyaandreev.test.models.User;
import com.example.ilyaandreev.test.models.responses.ServerResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ApiDataClient implements ApiDataInterface {

    public static final String BASE_URL = "https://api.github.com/";
    private ApiClient apiClient;


    @Inject
    public ApiDataClient() {
        apiClient = createApiClient(createOkHttpClient());
    }


    private ApiClient createApiClient(OkHttpClient client) {
        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());
        ObjectMapper jackson = new ObjectMapper();
        jackson.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(JacksonConverterFactory.create(jackson))
                .addCallAdapterFactory(rxAdapter)
                .build();
        return retrofit.create(ApiClient.class);
    }


    private OkHttpClient createOkHttpClient() {
        final OkHttpClient.Builder builder = new OkHttpClient().newBuilder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        return builder.build();
    }


    @Override
    public Observable<ServerResponse> searchUsers(String search) {
        Map<String, String> params = new HashMap<>();
        params.put("q", search);

        return apiClient.searchUser(params).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }


    @Override
    public Observable<List<Repository>> getRepositories(String userName) {
        return apiClient.getRepositories(userName).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }


    @Override
    public Observable<User> getUser(String userName) {
        return apiClient.getUser(userName).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
