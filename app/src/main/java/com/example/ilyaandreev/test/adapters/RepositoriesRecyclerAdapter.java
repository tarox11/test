package com.example.ilyaandreev.test.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ilyaandreev.test.R;
import com.example.ilyaandreev.test.models.Repository;


public class RepositoriesRecyclerAdapter extends BaseRecyclerAdapter<Repository> {


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.repository_lisitem, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ViewHolder holder = (ViewHolder) viewHolder;
        holder.bind(i);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView repoName, txtDescription, txtLang;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtDescription = itemView.findViewById(R.id.txtDescription);
            txtLang = itemView.findViewById(R.id.txtLang);
            repoName = itemView.findViewById(R.id.repoName);


        }

        public void bind(final int position) {

            Repository repository = getItem(position);
            repoName.setText(repository.getName());
            txtDescription.setText(repository.getDescription());
            txtLang.setText(repository.getLanguage());

        }
    }
}
