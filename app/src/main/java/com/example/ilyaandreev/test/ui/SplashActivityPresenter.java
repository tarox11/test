package com.example.ilyaandreev.test.ui;

import com.example.ilyaandreev.test.mvp.implementation.BasePresenterImpl;
import com.example.ilyaandreev.test.tools.SimpleTimer;

import javax.inject.Inject;

public class SplashActivityPresenter extends BasePresenterImpl<SplashActivityEvents.View> implements SplashActivityEvents.Presenter, SimpleTimer.SimpleTimerEvents {


    private SimpleTimer timer;


    @Inject
    public SplashActivityPresenter() {

    }


    @Override
    public void resume(SplashActivityEvents.View view) {
        super.resume(view);

        startTimer();

    }


    private void startTimer() {
        timer = new SimpleTimer(2000, 1000);
        timer.setListener(this);
        timer.start();
    }

    @Override
    public void destroy() {
        super.destroy();
        if (timer != null) {
            timer.setListener(null);
            timer.cancel();
        }
    }


    @Override
    public void timerStarted() {

    }

    @Override
    public void timerTicked(int timeLeft, int tickLeft) {

    }

    @Override
    public void timerDone() {
        getView().showMainScreen();
    }
}
