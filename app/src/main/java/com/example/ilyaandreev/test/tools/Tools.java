package com.example.ilyaandreev.test.tools;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

import com.example.ilyaandreev.test.interfaces.AlertEvents;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;

public class Tools {

    public static boolean isEmpty(Collection collection) {
        if (collection == null)
            return true;
        return collection.isEmpty();
    }

    public static String prepareDate(String dateIn) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");

        try {
            return sdf1.format(sdf.parse(dateIn));
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("bad pattern");
        }
        return dateIn;
    }

    public static void showAlert(Context context, String title, String message, String positive, String negative, final AlertEvents listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (!TextUtils.isEmpty(title))
            builder.setTitle(title);
        if (!TextUtils.isEmpty(message))
            builder.setMessage(message);
        if (!TextUtils.isEmpty(positive))
            builder.setPositiveButton(positive, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    listener.onPositiveClick(which);
                    dialog.dismiss();
                }
            });
        if (!TextUtils.isEmpty(negative))
            builder.setNegativeButton(negative, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    listener.onNegative();
                }
            });
        builder.create().show();

    }

}
