package com.example.ilyaandreev.test.ui;


import android.os.Bundle;

import com.example.ilyaandreev.test.R;
import com.example.ilyaandreev.test.ui.base.BaseActivity;
import com.example.ilyaandreev.test.ui.users.UserListFragment;

public class MainActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        replaceFragment(UserListFragment.newInstance());

    }


    @Override
    protected void onResume() {
        super.onResume();

    }


}
