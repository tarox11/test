package com.example.ilyaandreev.test.ui.users;

import com.example.ilyaandreev.test.models.Repository;
import com.example.ilyaandreev.test.models.User;
import com.example.ilyaandreev.test.mvp.interfaces.BasePresenter;
import com.example.ilyaandreev.test.mvp.interfaces.BaseView;

import java.util.List;

public interface UserListEvents {

    interface View extends BaseView {

        void showConnectionError();

        void showUsers(List<User> users);

    }

    interface Presenter extends BasePresenter<View> {
        void searchUser(String search);

        List<User> getUsers();
    }
}
