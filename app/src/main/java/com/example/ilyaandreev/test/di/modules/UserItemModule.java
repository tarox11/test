package com.example.ilyaandreev.test.di.modules;

import com.example.ilyaandreev.test.di.scopes.FragmentScope;
import com.example.ilyaandreev.test.ui.users.UserItemEvents;
import com.example.ilyaandreev.test.ui.users.UserItemFragment;
import com.example.ilyaandreev.test.ui.users.UserItemPresenter;


import dagger.Binds;
import dagger.Module;

@Module
public abstract  class UserItemModule {

    @Binds
    @FragmentScope
    public abstract UserItemEvents.Presenter presenter(UserItemPresenter presenter);

    @Binds
    public abstract UserItemEvents.View getFragment(UserItemFragment fragment);
}
