package com.example.ilyaandreev.test.network;


import com.example.ilyaandreev.test.models.Repository;
import com.example.ilyaandreev.test.models.User;
import com.example.ilyaandreev.test.models.responses.ServerResponse;

import java.util.List;
import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import rx.Observable;

public interface ApiClient {

    @GET("search/users")
    Observable<ServerResponse> searchUser(@QueryMap Map<String, String> params);

    @GET("users/{userName}")
    Observable<User> getUser(@Path("userName") String userName);

    @GET("users/{userName}/repos")
    Observable<List<Repository>> getRepositories(@Path("userName") String userName);


}
