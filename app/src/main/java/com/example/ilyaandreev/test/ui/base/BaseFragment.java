package com.example.ilyaandreev.test.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.example.ilyaandreev.test.mvp.interfaces.BaseView;

public class BaseFragment extends Fragment implements BaseView {


    protected BaseActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activity = (BaseActivity) getActivity();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void showError(String string) {
        activity.showError(string);
    }

    @Override
    public void showError(int resId) {
        activity.showError(resId);
    }

    @Override
    public void showProgress() {
        activity.showProgress();
    }

    @Override
    public void hideProgress() {
        activity.hideProgress();
    }

    @Override
    public void hideKeyboard() {
        activity.hideKeyboard();
    }

    @Override
    public void showNoConnection() {

    }



}
