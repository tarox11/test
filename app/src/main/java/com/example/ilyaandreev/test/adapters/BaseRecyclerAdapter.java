package com.example.ilyaandreev.test.adapters;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    public interface Events<T> {
        void onItemClick(T item, int position);
    }

    protected List<T> data;

    public void setItems(List<T> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void addItems(List<T> data) {
        if (this.data != null) {
            this.data.addAll(data);
        } else {
            this.data = data;
        }
        notifyDataSetChanged();
    }

    public void addItemAt(T item, int position) {
        if (data == null) {
            data = new ArrayList<>();
        }
        data.add(position, item);
        notifyItemInserted(position);
    }

    public void appendItem(T item) {
        if (data == null) {
            data = new ArrayList<>();
        }
        data.add(item);
        notifyItemInserted(data.size() - 1);
    }

    @Override
    public int getItemCount() {
        if (data != null) {
            return data.size();
        }
        return 0;
    }

    public T getItem(int position) {
        if (data != null) {
            return data.get(position);
        }
        return null;
    }

    public List<T> getItems() {
        return data;
    }

    public T getLastItem() {
        if (getItemCount() == 0)
            return null;
        return (getItem(getItemCount() - 1));
    }

    public T getFirstItem() {
        if (getItemCount() == 0)
            return null;
        return getItem(0);
    }
}
