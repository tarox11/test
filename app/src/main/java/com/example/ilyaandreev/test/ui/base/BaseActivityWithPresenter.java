package com.example.ilyaandreev.test.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.ilyaandreev.test.mvp.interfaces.BasePresenter;

import javax.inject.Inject;

public class BaseActivityWithPresenter<P extends BasePresenter> extends BaseActivity {

    @Inject
    public P presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.attachView(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.resume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()) {
            presenter.destroy();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.detachView(this);
    }


}
