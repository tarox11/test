package com.example.ilyaandreev.test.ui.users;

import com.example.ilyaandreev.test.models.Repository;
import com.example.ilyaandreev.test.models.User;
import com.example.ilyaandreev.test.mvp.interfaces.BasePresenter;
import com.example.ilyaandreev.test.mvp.interfaces.BaseView;

import java.util.List;

public interface UserItemEvents {

    interface View extends BaseView {
        void showRepositories(List<Repository> repositories);
        void showUser(User user);

    }

    interface Presenter extends BasePresenter<View> {

        void loadRepository(String userName);
        void loadUser(String userName);

        void saveUser();



    }
}
