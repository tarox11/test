package com.example.ilyaandreev.test.di.modules;


import android.content.Context;

import com.example.ilyaandreev.test.di.scopes.ActivityScope;
import com.example.ilyaandreev.test.interfaces.ApiDataInterface;
import com.example.ilyaandreev.test.interfaces.DatabaseHelperInterface;
import com.example.ilyaandreev.test.network.ApiDataClient;
import com.example.ilyaandreev.test.tools.DatabaseHelper;
import com.example.ilyaandreev.test.ui.MainActivity;
import com.example.ilyaandreev.test.ui.SplashActivity;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Module(includes = {AndroidSupportInjectionModule.class})
abstract  public class AppModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = {SplashActivityModule.class})
    abstract SplashActivity splashActivityInjector();

    @ActivityScope
    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    abstract MainActivity mainActivityInjector();

    @Provides
    @Singleton
    public static ApiDataInterface apiDataInterface() {
        return new ApiDataClient();
    }

    @Provides
    @Singleton
    public static DatabaseHelperInterface databaseHelperInterface(Context context) {
        return new DatabaseHelper(context);
    }



}
