package com.example.ilyaandreev.test.di.modules;

import com.example.ilyaandreev.test.di.scopes.FragmentScope;
import com.example.ilyaandreev.test.ui.users.UserListEvents;
import com.example.ilyaandreev.test.ui.users.UserListFragment;
import com.example.ilyaandreev.test.ui.users.UserListPresenter;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class UserListModule {

    @Binds
    @FragmentScope
    public abstract UserListEvents.Presenter presenter(UserListPresenter presenter);

    @Binds
    public abstract UserListEvents.View getFragment(UserListFragment fragment);




}
