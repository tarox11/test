package com.example.ilyaandreev.test.di.components;


import android.content.Context;

import com.example.ilyaandreev.test.core.TestApplication;
import com.example.ilyaandreev.test.di.modules.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder context(Context context);

        AppComponent build();
    }

    void inject(TestApplication app);
}
