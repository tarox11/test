package com.example.ilyaandreev.test.ui.users;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ilyaandreev.test.R;
import com.example.ilyaandreev.test.adapters.RepositoriesRecyclerAdapter;
import com.example.ilyaandreev.test.interfaces.AlertEvents;
import com.example.ilyaandreev.test.models.Repository;
import com.example.ilyaandreev.test.models.User;
import com.example.ilyaandreev.test.tools.Tools;
import com.example.ilyaandreev.test.ui.base.BaseFragmentWithPresenter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserItemFragment extends BaseFragmentWithPresenter<UserItemEvents.Presenter> implements UserItemEvents.View {


    private View v;


    TextView txtName;
    TextView txtLocation;
    ImageView imgUser;
    RecyclerView rv;

    RepositoriesRecyclerAdapter adapter;

    String userName;


    public static UserItemFragment newInstance(Bundle bundle) {
        UserItemFragment fragment = new UserItemFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userName = getArguments().getString("userName");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (v == null) {
            v = inflater.inflate(R.layout.user_item_frg, null);
            txtName = v.findViewById(R.id.txtName);
            txtLocation = v.findViewById(R.id.txtLocation);
            imgUser = v.findViewById(R.id.imgUser);
            rv = v.findViewById(R.id.rv);
            init();
        }

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.user_menu, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                Tools.showAlert(getContext(), null, getString(R.string.save), "OK", null, new AlertEvents() {
                    @Override
                    public void onPositiveClick(int which) {

                    }

                    @Override
                    public void onNegative() {

                    }
                });
                presenter.saveUser();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void init() {
        presenter.attachView(this);
        presenter.loadUser(userName);
        presenter.loadRepository(userName);

        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        adapter = new RepositoriesRecyclerAdapter();
        rv.setLayoutManager(manager);
        rv.setAdapter(adapter);


    }


    @Override
    public void showRepositories(List<Repository> repositories) {
        if (adapter == null) {
            adapter = new RepositoriesRecyclerAdapter();
        }
        adapter.setItems(repositories);
    }

    @Override
    public void showUser(User user) {
        txtName.setText(user.getLogin());
        txtLocation.setText(user.getLocation());
        Picasso.get().load(user.getAvatarUrl()).into(imgUser);
    }
}
