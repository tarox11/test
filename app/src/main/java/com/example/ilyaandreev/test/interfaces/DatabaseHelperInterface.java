package com.example.ilyaandreev.test.interfaces;

import com.example.ilyaandreev.test.models.Repository;
import com.example.ilyaandreev.test.models.User;

import java.util.List;

public interface DatabaseHelperInterface {

    void storeUser(User users);

    List<User> restoreUsers();

    List<Repository> restoreRepositories(String userId);

    void storeRepositories(List<Repository> repositories);

    User restoreUser(String userName);
}
