package com.example.ilyaandreev.test.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ilyaandreev.test.R;
import com.example.ilyaandreev.test.models.User;
import com.squareup.picasso.Picasso;

public class UsersRecyclerAdapter extends BaseRecyclerAdapter<User> {


    OnNewsItemClickListener listener;

    public interface OnNewsItemClickListener {
        void onItemClick(User user);


    }

    public void setOnNewClickListener(OnNewsItemClickListener listener) {
        this.listener = listener;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_listitem, viewGroup, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ViewHolder holder = (ViewHolder) viewHolder;
        holder.bind(i);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView image;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.imgUser);

            name = itemView.findViewById(R.id.txtName);


        }

        public void bind(final int position) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(getItem(position));
                }
            });
            User user = getItem(position);
            name.setText(user.getLogin());
            Picasso.get()
                    .load(user.getAvatarUrl())
                    .fit()
                    .centerCrop()
                    .into(image);


        }
    }


}
