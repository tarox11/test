package com.example.ilyaandreev.test.di.modules;

import com.example.ilyaandreev.test.di.scopes.ActivityScope;
import com.example.ilyaandreev.test.ui.SplashActivityEvents;
import com.example.ilyaandreev.test.ui.SplashActivityPresenter;

import dagger.Binds;
import dagger.Module;

@Module
public interface SplashActivityModule {

    @Binds
    @ActivityScope
    SplashActivityEvents.Presenter presenter(SplashActivityPresenter presenter);
}
