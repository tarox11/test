package com.example.ilyaandreev.test.mvp.interfaces;

public interface BaseView {

    void showError(String string);

    void showError(int resId);

    void showProgress();

    void hideProgress();

    void hideKeyboard();

    void showNoConnection();
}
