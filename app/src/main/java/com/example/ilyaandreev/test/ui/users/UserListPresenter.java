package com.example.ilyaandreev.test.ui.users;

import com.example.ilyaandreev.test.models.User;
import com.example.ilyaandreev.test.models.responses.ResponseHandler;
import com.example.ilyaandreev.test.models.responses.ServerResponse;
import com.example.ilyaandreev.test.mvp.implementation.BasePresenterImpl;
import com.example.ilyaandreev.test.tools.Tools;

import java.util.List;

import javax.inject.Inject;

public class UserListPresenter extends BasePresenterImpl<UserListEvents.View> implements UserListEvents.Presenter {

    @Inject
    public UserListPresenter() {

    }


    @Override
    public void searchUser(String search) {

        replaceLoadOperation(new ResponseHandler<ServerResponse>() {
            @Override
            public void success(ServerResponse response) {
                if (!Tools.isEmpty(response.users)) {
                    getView().showUsers(response.users);
                }
            }

            @Override
            public void businessError(ServerResponse response) {

            }

            @Override
            public void connectionError(String message) {
                getView().showConnectionError();

            }
        }, apiDataClient.searchUsers(search), true, true);


    }

    @Override
    public List<User> getUsers() {
        return databaseHelper.restoreUsers();
    }


}
