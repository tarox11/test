package com.example.ilyaandreev.test.tools;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ilyaandreev.test.interfaces.DatabaseHelperInterface;
import com.example.ilyaandreev.test.models.Repository;
import com.example.ilyaandreev.test.models.User;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class DatabaseHelper implements DatabaseHelperInterface {


    private final String USER_TABLE = "user_table";
    private final String REPOSITORY_TABLE = "repository_table";
    private final String DATABASE_FILENAME = "test";
    private final int VERSION = 1; //TODO увеличить при изменении структуры БД

    private Context context;
    private SQLiteDatabase db;
    private SQLiteOpenHelper helper;


    private void createUserTable(SQLiteDatabase db) {

        db.execSQL("create table " + USER_TABLE + " ("
                + "_id integer primary key autoincrement,"
                + "login text,"
                + "avatarUrl text,"
                + "location text"
                + ");");


    }

    private void createRepositoryTable(SQLiteDatabase db) {

        db.execSQL("create table " + REPOSITORY_TABLE + " ("
                + "_id integer primary key autoincrement,"
                + "id integer unique,"
                + "description text,"
                + "name text,"
                + "language text,"
                + "userId text"
                + ");");


    }


    @Inject
    public DatabaseHelper(Context context) {
        this.context = context;
        helper = new SQLiteOpenHelper(context, DATABASE_FILENAME, null, VERSION) {
            @Override
            public void onCreate(SQLiteDatabase db) {
                createUserTable(db);
                createRepositoryTable(db);

            }

            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                if (oldVersion < newVersion) {
                    db.delete(USER_TABLE, null, null);
                    db.delete(REPOSITORY_TABLE, null, null);
                }
            }
        };
        this.db = helper.getWritableDatabase();
    }


    @Override
    public void storeUser(User user) {
//            + "_id integer primary key autoincrement,"
//                    + "login text,"
//                    + "avatarUrl text,"
//                    + "location text"
//                    + ");");

        ContentValues cv = new ContentValues();
        cv.put("login", user.getLogin());
        cv.put("avatarUrl", user.getAvatarUrl());
        cv.put("location", user.getLocation());
        if (db.update(USER_TABLE, cv, "login=?", new String[]{user.getLogin()}) == 0) {
            db.insert(USER_TABLE, null, cv);
        }


    }


    private void storeRepository(Repository repository) {
//         + "_id integer primary key autoincrement,"
//                + "id integer unique,"
//                + "description text,"
//                + "name text,"
//                + "language text,"
//                + "userId text"
        ContentValues cv = new ContentValues();
        cv.put("id", repository.getId());
        cv.put("description", repository.getDescription());
        cv.put("language", repository.getLanguage());
        cv.put("userId", repository.getUser().getLogin());
        cv.put("name", repository.getName());
        if (db.update(REPOSITORY_TABLE, cv, "id=?", new String[]{String.valueOf(repository.getId())}) == 0) {
            db.insert(REPOSITORY_TABLE, null, cv);
        }

    }

    @Override
    public void storeRepositories(List<Repository> repositories) {
        for (Repository repository : repositories) {
            storeRepository(repository);
        }
    }

    @Override
    public User restoreUser(String userName) {
        Cursor c = db.query(USER_TABLE, null, "login=?", new String[]{userName}, null, null, null);
        User user = null;
        if (c.moveToFirst()) {
            user = prepareUser(c);
        }
        c.close();
        return user;
    }


    @Override
    public List<User> restoreUsers() {
        Cursor c = db.query(USER_TABLE, null, null, null, null, null, null);
        List<User> users = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                users.add(prepareUser(c));
            } while (c.moveToNext());
        }
        c.close();
        return users;
    }

    @Override
    public List<Repository> restoreRepositories(String userId) {
        Cursor c = db.query(REPOSITORY_TABLE, null, "userId=?", new String[]{userId}, null, null, null);
        List<Repository> repositories = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                repositories.add(prepareRepository(c));
            } while (c.moveToNext());
        }
        c.close();
        return repositories;
    }


    private Repository prepareRepository(Cursor c) {
//        + "_id integer primary key autoincrement,"
//                + "id integer unique,"
//                + "description text,"
//                + "name text,"
//                + "language text,"
//                + "userId text"
        Repository repository = new Repository();
        repository.setDescription(c.getString(c.getColumnIndex("description")));
        repository.setName(c.getString(c.getColumnIndex("name")));
        repository.setLanguage(c.getString(c.getColumnIndex("language")));
        return repository;
    }

    private User prepareUser(Cursor c) {
        User user = new User();
        user.setLogin(c.getString(c.getColumnIndex("login")));
        user.setAvatarUrl(c.getString(c.getColumnIndex("avatarUrl")));
        user.setLocation(c.getString(c.getColumnIndex("location")));
        return user;
    }


//    @Override
//    public void storeNews(List<New> news) {
//        for (New aNew : news) {
//            storeNew(aNew);
//        }
//    }
//
//
//    private void storeNew(New news) {
//        //        db.execSQL("create table " + NEWS_TABLE + " ("
////                + "_id integer primary key autoincrement,"
////                + "author text,"
////                + "title text,"
////                + "description text,"
////                + "url text,"
////                + "urlToImage text,"
////                + "publishedAt text,"
////                + "content text"
////                + ");");
//        ContentValues cv = new ContentValues();
//        cv.put("author", news.getAuthor());
//        cv.put("content", news.getContent());
//        cv.put("description", news.getDescription());
//        cv.put("publishedAt", news.getPublishedAt());
//        cv.put("title", news.getTitle());
//        cv.put("id", news.getSource().getId());
//        cv.put("name", news.getSource().getName());
//        db.insert(NEWS_TABLE, null, cv);
//    }
//
//
//
//
//    @Override
//    public List<New> restoreNews() {
//        List<New> news = new ArrayList<>();
//        Cursor c = db.query(NEWS_TABLE, null, null, null, null, null, null);
//        if (c.moveToFirst()) {
//            do {
//                news.add(prepareNew(c));
//            } while (c.moveToNext());
//        }
//        c.close();
//        return news;
//    }
//
//    private New prepareNew(Cursor c) {
//        New news = new New();
//        Source source=new Source();
//        news.setAuthor(c.getString(c.getColumnIndex("author")));
//        news.setContent(c.getString(c.getColumnIndex("content")));
//        news.setTitle(c.getString(c.getColumnIndex("title")));
//        news.setDescription(c.getString(c.getColumnIndex("description")));
//        news.setUrl(c.getString(c.getColumnIndex("url")));
//        news.setUrlToImage(c.getString(c.getColumnIndex("urlToImage")));
//        news.setPublishedAt(c.getString(c.getColumnIndex("publishedAt")));
//        source.setId(c.getString(c.getColumnIndex("id")));
//        source.setName(c.getString(c.getColumnIndex("name")));
//        news.setSource(source);
//        return news;
//    }
//
//


}
