package com.example.ilyaandreev.test.models.responses;

public interface ResponseHandler<T> {

    void success(T response);

    void businessError(T response);

    void connectionError(String message);
}
