package com.example.ilyaandreev.test.models;


import com.example.ilyaandreev.test.tools.Tools;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class User implements Serializable {


    @JsonProperty("avatar_url")
    private String avatarUrl;

    @JsonProperty("login")
    private String login;

    @JsonProperty("location")
    private String location;



    public String getAvatarUrl() {
        if (avatarUrl == null) {
            return "null";
        }
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getLogin() {
        if (login == null) {
            return "null";
        }
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLocation() {
        if (location == null) {
            return "null";
        }
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}