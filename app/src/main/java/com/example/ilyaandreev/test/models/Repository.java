package com.example.ilyaandreev.test.models;



import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


public class Repository implements Serializable {

    @JsonProperty("id")
    private long id;

    @JsonProperty("owner")
    private User user;

    @JsonProperty("language")
    private String language;

    @JsonProperty("name")
    private String name;


    @JsonProperty("description")
    private String description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getLanguage() {
        if (language == null) {
            return "";
        }
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getName() {
        if (name == null) {
            return "";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        if (description == null) {
            return "";
        }
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



}