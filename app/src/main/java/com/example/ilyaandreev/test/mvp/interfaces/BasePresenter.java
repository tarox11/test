package com.example.ilyaandreev.test.mvp.interfaces;

public interface BasePresenter<V> {

    void attachView(V view);

    void detachView(V view);

    void resume(V view);

    void destroy();

    boolean viewIsReady();

    V getView();
}
